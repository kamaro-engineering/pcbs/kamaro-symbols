# Kamaro Engineering e.V. Symbols

This repository contains the official Kamaro Engineering e.V. schematic symbol libraries.

**The libraries in this repository are intended for KiCad version 6.x**

Each symbol library is stored as a `.kicad_sym` file.

Contribution guidelines can be found at http://kicad.org/libraries/contribute
The library convention can be found at http://kicad.org/libraries/klc/

Other KiCad library repositories are located:

* Footprints: https://gitlab.com/kamaro-engineering/pcbs/kamaro-footprints
* 3D Models: https://gitlab.com/kamaro-engineering/pcbs/kamaro-packages3d
* Templates: https://gitlab.com/kamaro-engineering/pcbs/kamaro-templates
